import React, { useContext } from 'react'
import { InfoContext } from '../../Nitan'

const LearnUseContext2 = () => {
    let data = useContext(InfoContext)
  return (
    <div>
        {data}
      LearnUseContext2
    </div>
  )
}

export default LearnUseContext2
