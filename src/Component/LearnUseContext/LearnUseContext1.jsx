import React, { useContext } from 'react'
import { InfoContext } from '../../Nitan'

const LearnUseContext1 = () => {
    let name = useContext(InfoContext)
  return (
    <div>
     Name is   {name}
      LearnUseContext1
    </div>
  )
}

export default LearnUseContext1
