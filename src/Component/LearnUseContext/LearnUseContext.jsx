import React, { useContext } from 'react'
import LearnUseContext1 from './LearnUseContext1'
import {  InfoContext2, Infodata } from '../../Nitan'

const LearnUseContext = () => {
    let c1 = useContext(InfoContext2)
    let infoname = useContext(Infodata)


  return (

    <div>
      count= {c1.count}
      <br/>

      {infoname}
   
 
     < LearnUseContext1/>
    </div>
  )
}

export default LearnUseContext
