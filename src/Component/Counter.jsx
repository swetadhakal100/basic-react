import React, { useState } from 'react'

const Counter = () => {
    let [count,setCount] = useState(0)
  return (
    <div>
        {count}
    <button onClick = {()=>{
        setCount(count + 1)

    }}>Increment</button>
  <br/>

    <button onClick = {()=>{
        setCount(count - 1)

    }}>Decrement</button>
    <br/>

    <button onClick = {()=>{
        setCount(0)

    }}>Reset</button>


    </div>
  )
}

export default Counter
