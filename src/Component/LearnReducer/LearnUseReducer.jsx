import React, { useReducer } from 'react'

const LearnUseReducer = () => {
    let initialValue = 0
    let reducer = (state,action)=>{
        return 9
    }
    let [state,dispatch] = useReducer(reducer, initialValue)
  return (
    <div>
    <br/>
    {state}
    <br/>
    <button onClick={()=>{
        dispatch("ram")
    }}>
        Click me
    </button>
    </div>
  )
}

export default LearnUseReducer
