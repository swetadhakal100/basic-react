import React, { useReducer } from 'react'

const LearnUseReducer2 = () => {
    let initialValue = 0
   
    let reducer = (state,action)=>{
        //it must return next state value
        if(action.type === "Increment"){
            return state +1
        }
        else if (action.type === "Decrement"){
            return state - 1
        }
        else if (action.type === "Reset"){
            return initialValue
        }
        return state

    }
    let [count,dispatch] = useReducer(reducer,initialValue)
  
  return (
    <div>
        {count}
        <button onClick={()=>{
            dispatch({type:"Increment"})
        }}>Increment</button><br/>

        <button onClick={()=>{
              dispatch({type:"Decrement"})
        }}>Decrement</button><br/>

        <button onClick={()=>{
              dispatch({type:"Reset"})
        }}>Reset</button>
      
    </div>
  )
}

export default LearnUseReducer2
