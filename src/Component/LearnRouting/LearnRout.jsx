import React from 'react'
import { NavLink, Outlet, Route, Routes } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Contact from './Contact'
import Error from './Error'
import GetDynamicRouteParameter from './GetDynamicRouteParameter'

const LearnRout = () => {
    return (
        <div>
            {/* <a href='/about'>about</a>
        <br></br>
        / localhost:3000
        /about localhost:3000/about */}
            <NavLink to='/' style={{ marginLeft: "20px" }}>Home</NavLink>
            <NavLink to='/about' style={{ marginLeft: "20px" }}>About</NavLink>
            <NavLink to='/contact' style={{ marginLeft: "20px" }}>Contact</NavLink>

            <Routes>
                <Route path='/' element={<Home />}></Route>
                <Route path='/about' element={<About />}> </Route>
                <Route path='/contact' element={<Contact />}> </Route>
                <Route path='*' element={<Error />}> </Route>
               <Route path='/a' element={<div>a page</div>}> </Route>
                <Route path='/a/a1' element={<div>a1 page</div>}> </Route>
                <Route path='/a/a1/a2' element={<div>a a2 page</div>}> </Route>
                <Route path='/a/:any' element={<div>any page</div>}> </Route>
                <Route path='/b/:id1/id/:id2' element={<GetDynamicRouteParameter />}> </Route>




                {/* example of nesting routing
                //nesting means parent ra child ko relationn maintain garne..
                //main vannale hajurbuwa dekhi baba baba dekhi xori tasto reltion main garne lai nesting
                //rule 1:path should not be / 
                //rule2: parent must be Outlet tag 

                <Route path='a' element={<div>
                  <Outlet></Outlet>
                </div>}> 
                <Route path='a1'
                element={<div>a a2<Outlet></Outlet></div>} >
                <Route path='a2' element={<div>a a2 page</div>}>
                </Route>

                </Route>
                <Route path=':any'  element={<div>any page</div>}></Route>
                </Route> */}

            </Routes>

        </div>
    )
}

export default LearnRout
