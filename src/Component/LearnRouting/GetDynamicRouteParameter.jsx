import React from 'react'
import { useParams } from 'react-router-dom'

const GetDynamicRouteParameter = () => {
    //useParams() //it is used to get dynamic route param
    //param object ma hunxa console ma.
    //useSearchParams() //used to get query parameter
    //useNavigate() // used to change path onClick event
    const params = useParams()
  return (
    <div>
        GetDynamicRouteParameter
        <br></br>
        {params.id1}
        <br/>
        {params.id2}
    </div>
  )
}

export default GetDynamicRouteParameter
