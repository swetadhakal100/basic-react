import React from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'

const About = () => {
    const [searchParams] = useSearchParams()
    const navigate = useNavigate()
    return (
        <div>
            About page
            <br />
            name is {searchParams.get("name")}
            <br />
            age is  {searchParams.get("age")}
            <br />
            address is{searchParams.get("address")}
            <br/>
            <button onClick={()=>{ 
                // navigate("/contact")
                navigate("/contact", {replace:true})
            }}>Go to contact page</button>
        </div>
    )
}

export default About
