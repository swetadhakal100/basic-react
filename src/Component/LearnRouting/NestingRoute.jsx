import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom'

const NestingRoute = () => {
  //give / to home
  //donot give / to other
  return (
    <div>
      <Routes>
        <Route path='/' element={<div><Outlet></Outlet></div>}>
        <Route index element={<div>Home Page</div>}></Route>
        <Route path='student' element={<div><Outlet></Outlet></div>}>
          <Route index element={<div>Student Page</div>}></Route>
        <Route path='1' element={<div>1</div>}></Route>
        <Route path='kamal' element={<div>Kamal Pgae</div>}></Route>

        </Route>
        <Route path='*' element={<div>404 page</div>}></Route>

        </Route>
      </Routes>


      
      

    </div>
  )
}

export default NestingRoute
