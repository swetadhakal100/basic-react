import React, { useState } from 'react'

const Form3 = () => {
    let [name,setName] = useState("")
    let [age,setAge] = useState("")
    let [email,setEmail] = useState("")
    let [gender,setGender] = useState("")



  return (
    <form onSubmit={(e)=>{
    e.preventDefault()
    let data = {
        name,
        age,
        email,
        gender,
    }
    console.log(data);


    }
    }>
    <label htmlFor='name'>Name:</label>
    <input
    id='name'
     type='text'
      placeholder='Enter ur name'
    value={name}
    onChange={(e)=>{
        setName(e.target.value)
    }}
    ></input><br/> 

     <label htmlFor='age'>Age:</label>
    <input
     id='age' 
     type='number'
     placeholder='Enter ur age'
     value={age}
     onChange={(e)=>{
        setAge(e.target.value)
     }}></input><br/>


    <label htmlFor='email'>Email:</label>
    <input
     id='email'
     type='email'
     placeholder='Enter your email'
     value={email}
     onChange={(e)=>{
        setEmail(e.target.value)
     }}
     ></input><br/>

    <label htmlFor='male'>Gender:</label>
        <label htmlFor='male'>Male</label>
        <input type='radio' id='male' name='gender' value='male' onChange={(e)=>{
            setGender(e.target.value)
        }}></input>
        <label htmlFor='female'>Female</label>
        <input type='radio' id='female' name='gender' value='female' onChange={(e)=>{
            setGender(e.target.value)
        }}></input>
        <label htmlFor='other'>Other</label>
        <input type='radio' id='other' name='gender' value='other' onChange={(e)=>{
            setGender(e.target.value)
        }}></input>
    
    <br/>

    <button type='submit'>Send</button>
 

      
    </form>
  )
}

export default Form3
