import React, { useRef } from 'react'

const Learnref = () => {
    let inputRef1 = useRef()  
    let inputRef2 = useRef()  
    let inputRef3 = useRef()  
  return (
    <div>
      <p ref={inputRef1}>Hello P1</p>
      <p ref={inputRef2}>Hello P2</p>
      <input ref={inputRef3}></input>


      <button onClick={()=>{
        inputRef1.current.style.backgroundColor = "red"
        inputRef2.current.style.backgroundColor = "blue"
      }}>Change bg </button>

      <button onClick={()=>{
          inputRef3.current.focus()
      }}
      >Focus input</button>
      
    </div>
  )
}

export default Learnref
