import React, { useState } from 'react'

const Form4 = () => {
    let[productname,setName] = useState("")
    let[manageremail,setEmails] = useState("")
    let[staffemail,setEmail] = useState("")
    let [password,setPassword] = useState("")
    let [gender,setGender] = useState("")
    let [date,setDate] = useState("")
    let [married,setMarried] = useState(false)
    let [spouse,setManagerSpouse] = useState(" ")
    let [location,setLocation] = useState("")
    let [description,setDescription] = useState("")
    let [available,setAvailable] = useState("")


  return (

    <form onSubmit={(e)=>{
        e.preventDefault()
        let data = {
            productname,
            manageremail,
            staffemail,
            password,
            gender,
            date,
            married,
            spouse,
            location,
            description,
            available,
        }
        console.log(data);
    }}>

        <label htmlFor='productname'>productName:</label>
        <input
         id='productname'
          type='text'
          value={productname}
          onChange={(e)=>{
            setName(e.target.value)
          }}  
          ></input><br/>


        <label htmlFor='manageremail'>productManagerEmail</label>
        <input id='manageremail' type='email'
        value={manageremail}
        onChange={(e)=>{
            setEmails(e.target.value)
        }}
        ></input><br/>


        <label htmlFor='staffemail'>staffEmail</label>
        <input id='staffemail' type='email'
        value={staffemail}
        onChange={(e)=>{
            setEmail(e.target.value)
        }}  
        ></input><br/>


        <label htmlFor='password'>password</label>
        <input id='password' type='password'
        value={password}
        onChange={(e)=>{
            setPassword(e.target.value)
        }}
        ></input><br/>

        <label htmlFor='male'>gender: </label>
        
        <label htmlFor='male'>male</label>
        <input id='male' type='radio' name='gender' value='male'
        onChange={(e)=>{
            setGender(e.target.value)
        }}

        ></input>

        <label htmlFor='female'>female</label>
        <input id='female' type='radio' name='gender' value='female'
          onChange={(e)=>{
              setGender(e.target.value)
          }}></input><br/>

       
        <label htmlFor='date'>productManufactureDate</label>
        <input id='date' type='date'
        value={date}
        onChange={(e)=>{
            setDate(e.target.value)
        }}
        ></input><br/>

        <label htmlFor='married'>ManagerIsMarried</label>
        <input id='married' type='checkbox'
        checked={married}
        onChange={(e)=>{
            setMarried(e.target.checked)
        }}
        ></input><br/>

        <label htmlFor='spouse'>ManagerSpouse</label>
        <input id='spouse' type='text' 
        value={spouse}
        onChange={(e)=>{
            setManagerSpouse(e.target.value)
        }}
        ></input><br/>

        <label htmlFor='location'>productLocation</label>
        <input id='location' type='text'
        value={location}
        onChange={(e)=>{
            setLocation(e.target.value)
        }}
        ></input><br/>

        <label htmlFor='description'>productDescription</label>
        <input id='description' rows={5} cols={5}
        value={description}
        onChange={(e)=>{
            setDescription(e.target.value)
        }}
        ></input><br/>


        <label htmlFor='available'>productAvailable</label>
        <input id='available' type='number'
        value={available}
        onChange={(e)=>{
            setAvailable(e.target.value)
        }} 
        ></input><br/>

        <button type='submit'>send</button>


        


    </form>
      
    
  )
}

export default Form4
